# CREDENTIALS
APP_KEY = 'Replace with your Pushed API key'
APP_SECRET = 'Replace with your Pushed API secret'
CHANNEL_ALIAS = 'Replace with your Pushed channel alias'
CLAN_ID = "Replace wiyh your Clash Royale clan id" # Use with "%23XXXXXX" instead of "#XXXXXX"
API_TOKEN = "Replace wiyh your Clash Royale token API"
PASTEBIN_KEY = "Replace wiyh your Pastebin key"

# HEADER
HEADER = {
    "Accept": "application/json",
    "authorization": "Bearer " + API_TOKEN
}

# URLS
URL_BASE = "https://api.clashroyale.com/v1"

URL_MEMBERS = URL_BASE + "/clans/" + CLAN_ID + "/members"
URL_WARLOG = URL_BASE + "/clans/" + CLAN_ID + "/warlog"
URL_CURRENTWAR = URL_BASE + "/clans/" + CLAN_ID + "/currentwar"

# CONST

DATAFILE = "./CRStats.json"
DISCORD_MAX_MSG_LENGTH = 2000
TOKEN = "XXXXXXXXXXXXX"   # Get at discordapp.com/developers/applications/me
