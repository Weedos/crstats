#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

import json
import pushed
import asyncio
import aiohttp
from enum import Enum
from os.path import isfile
from urllib import request, parse
from collections import namedtuple
from settings import *
from termcolor import colored


class TypeDmsg(Enum):
    WARLOG = 0
    WAR = 1
    RISK = 2

class CRStats:
    def __init__(self):
        self.members = None
        self.warlog = None
        self.currentwar = None

    async def fetch_members(self):
        self.members = await self.fetch_url(URL_MEMBERS, True)

    async def fetch_url(self, url, items=None):
        async with aiohttp.ClientSession(headers=HEADER) as session:
            async with session.get(url) as resp:
                if resp.status == 200:
                    if items is not None:
                        return json2obj(json.dumps(json.loads(await resp.text())["items"]))
                    else:
                        return json2obj(await resp.text())

    async def fetch(self):
        await self.fetch_members()
        await self.fetch_warlog()
        await self.fetch_currentwar()

    async def fetch_warlog(self):
        self.warlog = await self.fetch_url(URL_WARLOG, True)

    async def fetch_currentwar(self):
        self.currentwar = await self.fetch_url(URL_CURRENTWAR)

    def get_member_current_war(self, wanted):
        for member in self.currentwar.participants:
            if wanted.tag == member.tag:
                return member
        return None

    def get_currentwar_log(self):
        if self.members is None or self.currentwar is None:
            return "Error Members table or currentwar table is None !"
        participants = 'State of war: '
        if self.currentwar.state == "warDay":
            participants += 'War Day.\n'
            for member in self.members:
                wanted = self.get_member_current_war(member)
                if wanted is not None:
                    if wanted.battlesPlayed == 0:
                        win = '%s has not played any games yet, ' % (wanted.name)
                    else:
                        win = '%s has won %d/%d, ' % (wanted.name, wanted.wins, wanted.battlesPlayed)
                    participants +=  win + "and played %d/3 preparation fights.\n" % (wanted.collectionDayBattlesPlayed)
                else:
                    participants += "%s did not participate in this war.\n" % (member.name)
        elif self.currentwar.state == "collectionDay":
            participants += 'Collection Day.\n'
            for member in self.members:
                wanted = self.get_member_current_war(member)
                if wanted is not None:
                    if wanted.collectionDayBattlesPlayed == 0:
                        win = '%s has not played any games yet, ' % (wanted.name)
                    else:
                        win = '%s has won %d/%d, ' % (wanted.name, wanted.wins, wanted.collectionDayBattlesPlayed)
                    participants +=  win + "and played %d/3 preparation fights.\n" % (wanted.collectionDayBattlesPlayed)
                else:
                    participants += "%s did not participate in this war.\n" % (member.name)
        else:
            participants += 'Not in War.\n\n'
        return participants

    def get_curwar_discord(self):
        return self.discord_msg_iter(self.get_currentwar_log(), TypeDmsg.WAR)

    def get_participation(self):
        if self.members is None:
            return "Error: Members table is None !"
        warlog = ''
        rating, n_wars = self.get_rating()
        for member, value in rating.items():
            r = (value * 100) / n_wars
            s = str(r)+"%"
            emoji = "👍🏽"
            if r < 40:
                emoji = "👎🏽"
            warlog += "%s => %s  %s\n"%(member, s, emoji)
        warlog += "Results on %d lasts wars.\n"%(n_wars)
        return warlog

    def get_part_discord(self):
        if self.members is None:
            return "Error: Members table is None !"
        warlog = ''        
        rating, n_wars = self.get_rating()
        for member, value in rating.items():
            r = (value * 100) / n_wars
            s = str(r)+"%"
            c = '+'
            emoji = "👍🏽"
            if r < 40:
                emoji = "👎🏽"
                c = '-'
            warlog += "%s%s => %s  %s\n"%(c, member, s, emoji)
        warlog += "Results on %d lasts wars.\n"%(n_wars)
        return self.discord_msg_iter(warlog + self.get_attendance(), TypeDmsg.WARLOG)

    def get_attendance(self):
        if self.members is None:
            return "Error: Members table is None !"
        rating, n_wars = self.get_rating()
        nb_heretics = 0
        for _, value in rating.items():
            r = (value * 100) / n_wars
            if r < 40:
                nb_heretics += 1
        s = ''
        if nb_heretics > 0:
            s = "%d members maybe left..."%(nb_heretics)
        else:
            s = "GG! All members have done !"
        return "On %d last wars results: %s.\n" % (n_wars, s)

    def get_rating(self):
        rating = {}
        for member in self.members:
            rating[member.name] = 0
            for i, war in enumerate(self.warlog):
                for player in war.participants:
                    if player.tag == member.tag:
                        rating[member.name] += 1
                n_wars = i + 1
        return rating, n_wars

    def print_participation(self):
        if self.members is None:
            return "Error: Members table is None !"
        rating = {}
        n_wars = 10
        for member in self.members:
            rating[member.name] = 0
            for i, war in enumerate(self.warlog):
                for player in war.participants:
                    if player.tag == member.tag:
                        rating[member.name] += 1
                n_wars = i + 1
        for member, value in rating.items():
            r = (value * 100) / n_wars
            s = str(r)+"%"
            c = "green"
            if r < 40:
                c = "red"
            print(member + " => ", colored(s, c))
        print("Results on %d lasts wars\n" % (n_wars))

    def get_header_msg(self, mtype):
        if mtype == TypeDmsg.WARLOG:
            return '```diff\n'
        elif mtype == TypeDmsg.WAR:
            return '```yaml\n'
        else:
            return '```\n'

    def discord_msg_iter(self, msg, mtype):
        header = self.get_header_msg(mtype)
        if len(msg) < DISCORD_MAX_MSG_LENGTH:
            yield header + msg + "```"
            return
        n_page = 1
        to_send = []
        while len(msg) > DISCORD_MAX_MSG_LENGTH - 12:
            n_page += 1
            occur = msg[:DISCORD_MAX_MSG_LENGTH - 12].rfind('\n')
            if occur > -1:
                to_send.append(msg[:occur + 1])
                msg = msg[occur + 1:]
            else:
                to_send = ['Error']
        to_send.append(msg)
        for i, m in enumerate(to_send):
            yield header + m + "Page: %d/%d\n```" % (i+1, n_page)




    def send_push_notif(self, url):
        notif = "%s\nMore details at %s" % (self.get_attendance(), url)
        p = pushed.Pushed(APP_KEY, APP_SECRET)
        shipment = p.push_channel(notif, CHANNEL_ALIAS)

    def send_log(self):
        args = {'api_option':'paste',
                'api_paste_code': self.get_participation(),
                'api_dev_key': PASTEBIN_KEY
            }
        url = 'https://pastebin.com/api/api_post.php'
        data = parse.urlencode(args).encode()
        req = request.Request(url, data=data)
        response = request.urlopen(req)
        pastebin = response.read().decode()
        self.send_push_notif(pastebin)
        print("Pastebin: " + pastebin)


def _json_object_hook(d):
    return namedtuple('X', d.keys())(*d.values())

def json2obj(data):
    return json.loads(data, object_hook=_json_object_hook)

async def main():
    stats = CRStats()
    if isfile(DATAFILE):
        with open(DATAFILE, 'r') as file_cr:
            data = json.loads(file_cr.read())
            stats.__dict__ = data
    await stats.fetch()
    import time
    time.sleep(5)
    print(stats.get_currentwar_log())
    print(stats.get_attendance())
    print(stats.get_participation())
    stats.print_participation()
    with open(DATAFILE, 'w') as file_cr:
        json.dump(stats.__dict__, file_cr)
    #stats.send_log()

if __name__ == "__main__":
    print("Setting loop...")
    loop = asyncio.get_event_loop()
    print("Processing...")
    loop.run_until_complete(main())
    loop.close()
    print("Done !")